//
//  DBManager.swift
//  MEET
//
//  Created by Denis on 27.04.16.
//  Copyright © 2016 Denis Inc. All rights reserved.
//

import UIKit
import CoreData

public class DBManager: NSObject {
    
    // MARK: - Private Properties
    private var innerContext: NSManagedObjectContext?
    private var innerObjectModel: NSManagedObjectModel?
    private var innerCoordinator: NSPersistentStoreCoordinator?
    
    /** Name of DataBase File */
    private let managerModelName = "FuelCell"
    
    /** Convenience property to get documents url */
    private var documentsURL: NSURL {
        get {
            let fileManager = NSFileManager.defaultManager()
            return fileManager.URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask).last!
        }
    }
    
    /** Convenience property to get DB store url */
    private var storeURL: NSURL {
        get {
            return documentsURL.URLByAppendingPathComponent("\(managerModelName).sqlite")
        }
    }
    
    // MARK: - Public Computed Properties
    /** Default context in the App */
    public var defaultContext: NSManagedObjectContext? {
        get {
            if let context = innerContext {
                return context
            }
            else {
                if let coordinator = persistentStoreCoordinator {
                    innerContext = NSManagedObjectContext(concurrencyType: .MainQueueConcurrencyType)
                    innerContext?.persistentStoreCoordinator = coordinator
                }
                return innerContext
            }
        }
    }
    
    /** Object Model used in the App */
    public var managedObjectModel: NSManagedObjectModel {
        get {
            if let model = innerObjectModel {
                return model
            }
            else {
                let modelURL = NSBundle.mainBundle().URLForResource(managerModelName, withExtension: "momd")!
                innerObjectModel = NSManagedObjectModel(contentsOfURL: modelURL)
                return innerObjectModel!
            }
        }
    }
    
    /** DB Persistent Store Coordinator */
    public var persistentStoreCoordinator: NSPersistentStoreCoordinator? {
        get {
            if let coordinator = innerCoordinator {
                return coordinator
            }
            else {
                innerCoordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
                do {
                    try innerCoordinator!.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL: storeURL, options: nil)
                }
                catch let error as NSError {
                    do {
                        print("Unresolved error in DBManager. Info:\nLine: \(#line)\nError: \(error)")
                        try NSFileManager.defaultManager().removeItemAtURL(storeURL)
                    }
                    catch let deleteError as NSError {
                        print("Error while trying to remove persistent store: \(deleteError)")
                    }
                    
                    abort()
                }
                
                return innerCoordinator
            }
        }
    }
    
    // MARK: Shared instance
    /** Shared Instance of the manager */
    public static let sharedManager = DBManager()
    
    // MARK: Class Computed Properties
    /** Convenience property for fast access to default managed object context of DB */
    public static var managedObjectContext: NSManagedObjectContext? {
        get {
            return DBManager.sharedManager.defaultContext
        }
    }
    
    // MARK: - Public Methods
    /** Save all changes in DB context */
    public func saveContext() -> Bool {
    
        if let context = defaultContext {
            if context.hasChanges {
                do {
                    try context.save()
                }
                catch let error as NSError {
                    print("Error while saving managed object context in DB Manager. Info:\nLine: \(#line)\nError: \(error)")
                    
                    do {
                        try NSFileManager.defaultManager().removeItemAtURL(storeURL)
                    }
                    catch let deleteError as NSError {
                        print("Error while trying to remove persistent store: \(deleteError)\nLine: \(#line)")
                    }
                    
                    abort()
                }
            }
        }
        
        return false
    }

}
