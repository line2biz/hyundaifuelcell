//
//  DMStation+CoreDataProperties.swift
//  FuelCell
//
//  Created by Шурик on 18.05.16.
//  Copyright © 2016 Alex. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension DMStation {

    @NSManaged var city: String?
    @NSManaged var countryCode: String?
    @NSManaged var email: String?
    @NSManaged var latitude: NSNumber?
    @NSManaged var longitude: NSNumber?
    @NSManaged var opening_time: String?
    @NSManaged var owner: String?
    @NSManaged var phone: String?
    @NSManaged var provider: String?
    @NSManaged var responsible_person: String?
    @NSManaged var stationID: NSNumber?
    @NSManaged var status: NSNumber?
    @NSManaged var street: String?
    @NSManaged var updated_at: String?
    @NSManaged var zip: String?
    @NSManaged var country: String?

}
