//
//  AZMapController.swift
//  FuelCell
//
//  Created by Шурик on 06.05.16.
//  Copyright © 2016 Alex. All rights reserved.
//

import UIKit
import MapKit
import CoreData
import SVProgressHUD


class AZMapController: UIViewController {
    
//    MARK: - Types
    struct MainStoryboard {
        struct Map {
            static let spanLatitude: Double = 9000
        }
        
        struct SegueIdentifiers {
            static let ShowDetailController = "ShowDetailController"
            static let ShowEmbedDetails = "ShowEmbedDetails"
        }
    }


//    MARK: Outlets
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var searcgTextField: UITextField!
    @IBOutlet weak var currentLocationButton: UIButton!
    @IBOutlet weak var searchBar: UIView!
    @IBOutlet var containerView: UIView!
    @IBOutlet var listContainerView: UIView!
    
    
    @IBOutlet var menuButton: UIBarButtonItem!
    @IBOutlet var backButton: UIBarButtonItem!
    
    
    
    @IBOutlet weak var mapBottomConstraint: NSLayoutConstraint!
    
//    MARK: Public properties
    var managedObjectContext: NSManagedObjectContext? = DBManager.sharedManager.defaultContext
    var annotationType: AnnotationType = .Station
    
//    MARK: Private properties
    private var selectionTimer: NSTimer?
    private var detailController: ZNDetailsController?
    private var annotationsController: AZAnnotationsController?
    private var previousRegion: MKCoordinateRegion?
    
//    MARK: - View Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        listContainerView.hidden = true
        
//        displayPins()
        
        // Hide info on start
        mapBottomConstraint.constant = 0
        
        // Location manager
//        locationManager.desiredAccuracy = kCLLocationAccuracyBest;
//        locationManager.delegate = self;
//        
//        locationManager.requestWhenInUseAuthorization()
//        
//        
//        
        
//        locationManager.startUpdatingLocation()
//        locationManager.startUpdatingHeading()
//        
        
        
        // Map view configuration
        mapView.showsUserLocation = true
        mapView.mapType = .Standard
        mapView.showsCompass = false
//        mapView.userTrackingMode = .FollowWithHeading
        
        
//        locationManager.startMonitoringSignificantLocationChanges()
        
//         Center the map on Berlin
//        let BerlinCoordinates = CLLocationCoordinate2DMake(52.518635, 13.401150);
//        let viewRegion = MKCoordinateRegionMakeWithDistance(BerlinCoordinates, 9000, 9000);
//        let adjustedRegion =  mapView.regionThatFits(viewRegion)
//        mapView.setRegion(adjustedRegion, animated: true)
//
//        MyCustomPointAnnotation* point1 = [[MyCustomPointAnnotation alloc] init];
//        point1.coordinate = CLLocationCoordinate2DMake(42.3601, -71.0589);
//        point1.price = 3;
//        [self.mapView addAnnotation:point1];
//        
//        MyCustomPointAnnotation* point2 = [[MyCustomPointAnnotation alloc] init];
//        point2.coordinate = CLLocationCoordinate2DMake(42.3606, -71.0583);
//        point2.price = 5;
//        [self.mapView addAnnotation:point2];
        
//        var fetchRequest: NSFetchRequest
//        
//        switch self.annotationType {
//        case .Station:
//            fetchRequest = NSFetchRequest(entityName: "DMStation")
//            break;
//        case .Dealer:
//            fetchRequest = NSFetchRequest(entityName: "DMDealer")
//            break;
//        }
//        
//        if let results = try! managedObjectContext!.executeFetchRequest(fetchRequest) as? [NSManagedObject] {
//            if results.count > 2 {
//                for idx in 0...results.count / 2 {
//                    managedObjectContext?.deleteObject(results[idx])
//                }
//            }
//            
//        }
//        
//        
//        
//        
//        let delay = 3 * Double(NSEC_PER_SEC)
//        let time = dispatch_time(DISPATCH_TIME_NOW, Int64(delay))
//        dispatch_after(time, dispatch_get_main_queue(), {
//            self.loadDataFromServer();
//        })
        
        self.reloadData()
        self.loadDataFromServer();
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        // Invalidate timer
        if let _ = selectionTimer {
            selectionTimer!.invalidate()
            selectionTimer = nil
        }
    }
    
    
//    MARK: - Outlet methods
    
    @IBAction func didTapButtonCurrentLocation(sender: AnyObject) {
        
        if let location = mapView.userLocation.location {
//            mapView.setCenterCoordinate(location.coordinate, animated: true)
            
            
            let region = MKCoordinateRegionMakeWithDistance(location.coordinate, 400, 400)
            let adjustedRgion = self.mapView.regionThatFits(region)
            self.mapView.setRegion(adjustedRgion, animated: true)
            
            
        }
        
        
    }

    @IBAction func didTapButtonBack(sender: AnyObject) {
        
        for annotation in mapView.annotations {
            mapView.deselectAnnotation(annotation, animated: true)
        }
        
        hideDetails()
    }
    
    
    @IBAction func didTapButtonShow(sender: AnyObject) {
        listContainerView.hidden = !listContainerView.hidden
        
        if listContainerView.hidden && searcgTextField.isFirstResponder() {
            searcgTextField.resignFirstResponder()
            searcgTextField.text = nil
        }
        
        if listContainerView.hidden == false {
            annotationsController?.userLocation = mapView.userLocation.location
        }
        
    }
    
//    MARK: - Public functions
    internal func  updateController()  {
        _fetchedResultsController = nil
        
        listContainerView.hidden = true
        
        if listContainerView.hidden && searcgTextField.isFirstResponder() {
            searcgTextField.resignFirstResponder()
            searcgTextField.text = nil
        }
        
        hideDetails()
        reloadData()
        loadDataFromServer()
        
    }
    
//    MARK: - Private functions
    
    private func reloadData() {
        _fetchedResultsController = nil
        // Remove all annotations from the map view
        mapView.removeAnnotations(mapView.annotations)
        
        if let arr = fetchedResultsController.fetchedObjects as? [MKAnnotation] {
            mapView.addAnnotations(arr)
        }

    }
    
    
    private func showDetails(view: MKAnnotationView) {
        if let mo = view.annotation as? NSManagedObject {
            detailController?.userCoordinate = mapView.userLocation.coordinate
            detailController?.item = mo
            
            let infoTableHeight  = (detailController?.height())!
            
            if let _ = selectionTimer {
                // Disable hidding the detail controller
                selectionTimer!.invalidate()
                selectionTimer = nil
                
            } else {
                
                // Replace 'menu' button with 'back' button
                navigationItem.setLeftBarButtonItem(backButton, animated: true)
                
                // Store region
                previousRegion = mapView.region
                
                
                if let crd = view.annotation?.coordinate {
                    
                    let lat = previousRegion?.span.latitudeDelta > MainStoryboard.Map.spanLatitude ? previousRegion?.span.latitudeDelta : MainStoryboard.Map.spanLatitude
                    
                    let region = MKCoordinateRegionMakeWithDistance(crd, lat!, lat!)
                    let adjustedRgion = self.mapView.regionThatFits(region)
                    self.mapView.setRegion(adjustedRgion, animated: false)
                }
                
                self.mapBottomConstraint.constant = infoTableHeight
                
                UIView.animateWithDuration(0.5, animations: {
                    self.view.layoutIfNeeded()
                    self.searchBar.alpha = 0
                    self.currentLocationButton.alpha = 0
                    
                    
                }) { (compeleted) in
                    if let crd = view.annotation?.coordinate {
                        let region = MKCoordinateRegionMakeWithDistance(crd, 400, 400)
                        let adjustedRgion = self.mapView.regionThatFits(region)
                        self.mapView.setRegion(adjustedRgion, animated: true)
                    }
                    
                }
                
            }
        }
    }
    
    func hideDetails() {
        
        if let _ = selectionTimer {
            selectionTimer?.invalidate()
            selectionTimer = nil
        }
        
        
        if let region = self.previousRegion {
            self.mapView.setRegion(region, animated: true)
        }
        
        navigationItem.setLeftBarButtonItem(menuButton, animated: true)
        
        self.mapBottomConstraint.constant = 0
        
        UIView.animateWithDuration(0.5, animations: {
            
             self.view.layoutIfNeeded()
//            self.mapView.frame.size.height = CGRectGetHeight(self.view.bounds)
//            self.containerView.frame.origin.y = CGRectGetHeight(self.view.bounds)
            
            
            self.searchBar.alpha = 1
            self.currentLocationButton.alpha = 1
            
        }) { (compeleted) in
//            self.mapBottomConstraint.constant = 0
            self.selectionTimer?.invalidate()
            self.selectionTimer = nil
            
            
        }
    }
    
    func loadDataFromServer() {
        
        let url: NSURL
        if annotationType == .Station {
            url = APIController.URL(forPath: "get-hydrogen-filling-stations")
        } else {
            url = APIController.URL(forPath: "get-dealers")
        }
        
        
        
        let configuration = NSURLSessionConfiguration.defaultSessionConfiguration()
        let session = NSURLSession(configuration: configuration)
        let request = NSURLRequest.authorithedRequest(url)
        
//        let request = NSURLRequest(URL: url)
        
        let task = session.dataTaskWithRequest(request) { (data, response, error) in
            
            if let _ = error {
                print("\(#function) error: \(error)")
            } else {
                
                do {
                    let json = try NSJSONSerialization.JSONObjectWithData(data!, options: .AllowFragments)
                    switch self.annotationType {
                    case .Station:
                        DMStation.parseResponse(json, inManagedObjectContext: self.managedObjectContext!)
                    case .Dealer:
                        DMDealer.parseResponse(json, inManagedObjectContext: self.managedObjectContext!)
                    }
                }
                catch let error as NSError {
                    print("\(#function) error: \(error)")
                }
                
            }
        }
        task.resume()
        
        
    }
    
    
//    MARK: - Navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if let detailVC = segue.destinationViewController as? ZNDetailsController {
           
            detailController = detailVC
        }
        else if let annotationVC = segue.destinationViewController as? AZAnnotationsController {
            annotationVC.delegate = self
            annotationVC.annotationType = annotationType
            annotationsController = annotationVC
        }
        
     }
    
//    MARK: - Fetched results controller
    var fetchedResultsController: NSFetchedResultsController {
        if _fetchedResultsController != nil {
            return _fetchedResultsController!
        }
        
        
        let fetchRequest = NSFetchRequest()
        // Edit the entity name as appropriate.
        var entity: NSEntityDescription
        
        switch self.annotationType {
        case .Station:
            entity  = NSEntityDescription.entityForName("DMStation", inManagedObjectContext: self.managedObjectContext!)!
        case .Dealer:
            entity = NSEntityDescription.entityForName("DMDealer", inManagedObjectContext: self.managedObjectContext!)!
        }
        fetchRequest.entity = entity
        
        // Set the batch size to a suitable number.
        fetchRequest.fetchBatchSize = 20
        
        // Edit the sort key as appropriate.
        let sortDescriptor = NSSortDescriptor(key: "zip", ascending: false)
        
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        // Edit the section name key path and cache name if appropriate.
        // nil for section name key path means "no sections".
        let aFetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: self.managedObjectContext!, sectionNameKeyPath: nil, cacheName: nil)
        aFetchedResultsController.delegate = self
        _fetchedResultsController = aFetchedResultsController
        
        do {
            try _fetchedResultsController!.performFetch()
        } catch {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            //print("Unresolved error \(error), \(error.userInfo)")
            abort()
        }
        
        return _fetchedResultsController!
    }
    var _fetchedResultsController: NSFetchedResultsController? = nil
}


//    MARK: - Annotation controller delegate
extension AZMapController: AZAnnotationsControllerDelegate {
    func annotationsController(annotationsController: AZAnnotationsController, didSelectObject object: NSManagedObject) {
        listContainerView.hidden = true
        searcgTextField.text = nil
        
        for pin in mapView.selectedAnnotations {
            mapView.deselectAnnotation(pin, animated: true)
        }
        
        if let annotation = object as? MKAnnotation {
            mapView.selectAnnotation(annotation, animated: true)
        }
        
        
        
    }
}


//    MARK: - Map View delegate
extension AZMapController: MKMapViewDelegate {
    

    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
        
        if let _ = annotation as? DMStation {
            let Identifier = "Station"
            var annotationView = mapView.dequeueReusableAnnotationViewWithIdentifier(Identifier) as? AZStationAnnotationView
            if annotationView == nil {
                annotationView = AZStationAnnotationView(annotation: annotation, reuseIdentifier: Identifier)
                annotationView?.canShowCallout = false
            } else {
                annotationView?.annotation = annotation
            }
            return annotationView
        }
        else if let _ = annotation as? DMDealer {
            let Identifier = "Dealer"
            var annotationView = mapView.dequeueReusableAnnotationViewWithIdentifier(Identifier) as? AZDealerAnnotationView
            if annotationView == nil {
                annotationView = AZDealerAnnotationView(annotation: annotation, reuseIdentifier: Identifier)
                annotationView?.canShowCallout = false
            } else {
                annotationView?.annotation = annotation
            }
            return annotationView
        }
        
        return nil
    }
    
    func mapViewWillStartLoadingMap(mapView: MKMapView) {
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
    }
    
    func mapViewDidFailLoadingMap(mapView: MKMapView, withError error: NSError) {
        UIApplication.sharedApplication().networkActivityIndicatorVisible = false
    }
    
    func mapViewDidFinishLoadingMap(mapView: MKMapView) {
        UIApplication.sharedApplication().networkActivityIndicatorVisible = false
    }
    
    
    func mapView(mapView: MKMapView, didSelectAnnotationView view: MKAnnotationView) {
        
//        print("\(#file.componentsSeparatedByString("/").last):\(#function)")
        
        showDetails(view)
    }
    
    func mapView(mapView: MKMapView, didDeselectAnnotationView view: MKAnnotationView) {
        selectionTimer = NSTimer.scheduledTimerWithTimeInterval(0.05, target: self, selector: #selector(AZMapController.hideDetails), userInfo: nil, repeats: false)
    }
    
    
    func mapView(mapView: MKMapView, didUpdateUserLocation userLocation: MKUserLocation) {
        AppDelegate.sharedDelegate().userLocation = userLocation.location
    }
    
    func mapView(mapView: MKMapView, regionWillChangeAnimated animated: Bool) {
        searcgTextField.resignFirstResponder()
    }
    
}

//    MARK: - Fetched Results controller delegate
extension AZMapController: NSFetchedResultsControllerDelegate  {
    
    func controller(controller: NSFetchedResultsController, didChangeObject anObject: AnyObject, atIndexPath indexPath: NSIndexPath?, forChangeType type: NSFetchedResultsChangeType, newIndexPath: NSIndexPath?) {
        switch type {
        case .Insert:
            if let annotation = anObject as? MKAnnotation {
                mapView.addAnnotation(annotation)
            }
        case .Delete:
            if let annotation = anObject as? MKAnnotation {
                mapView.removeAnnotation(annotation)
            }
        case .Update, .Move:
            if let annotation = anObject as? MKAnnotation {
                mapView.removeAnnotation(annotation)
                mapView.addAnnotation(annotation)
            }
        }
    }
    
}

//    MARK: - Text field delegate
extension AZMapController: UITextFieldDelegate {
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        print("\(#function) search text: \(textField.text)")
        
        if textField.hasText() {
            
            SVProgressHUD.showWithStatus(NSLocalizedString("Searching...", comment: ""))
            
            
//            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//                // do some task
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    // update some UI
//                    });
//                })

            
//            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
            
            let geocoder = CLGeocoder()
                geocoder.geocodeAddressString(textField.text!, completionHandler: { (placemarks, error) in
                    
                    //                if let placemark = placemarks?.first {
                    
                    
                    
                    if let _ = error {
                        print("error: \(error)")
                        dispatch_async(dispatch_get_main_queue(), { 
                            SVProgressHUD.showErrorWithStatus(NSLocalizedString("Not found", comment: ""))
                        })

                    } else {
                        SVProgressHUD.showInfoWithStatus(NSLocalizedString("Found", comment: ""))
                        
                        if let placemark = placemarks?.first {
                            self.annotationsController?.userLocation = placemark.location
                            self.listContainerView.hidden = false
                            
                            let address = placemark.addressDictionary!["FormattedAddressLines"] as! NSArray
                            textField.text = address.componentsJoinedByString(", ")
                            textField.resignFirstResponder()
                            
                            
                            
                        }
                        SVProgressHUD.showInfoWithStatus(NSLocalizedString("Found", comment: ""))
                        
//                        if let array = placemarks {
//                            
//                            
//                            
//                            for placemark in array {
//                                let address = placemark.addressDictionary!["FormattedAddressLines"] as! NSArray
//                                print("placemark: \(placemark.addressDictionary)\naddress: \(address)")
//                                
//                                
//                                
//                            }
//                        }
                    }
                    
                })
                
//            })
            
            
            
        }
        
        return true
    }
}

