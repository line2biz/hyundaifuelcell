//
//  AZStationAnnotationView.swift
//  FuelCell
//
//  Created by Шурик on 07.05.16.
//  Copyright © 2016 Alex. All rights reserved.
//

import UIKit
import MapKit

class AZStationAnnotationView: MKAnnotationView {

    var station: DMStation? {
        get {
            if let st = self.annotation as? DMStation {
                return st
            } else {
                return nil
            }
        }
    }
    
    override var annotation: MKAnnotation? {
        didSet {
            commonInit()
        }
    }
    
    override init(annotation: MKAnnotation?, reuseIdentifier: String?) {
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier)
        commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    private func commonInit() {
        
        var imageName = "Station-Blue"
        if station?.status == false {
            imageName = "Station-Red"
        }
        
        self.image = UIImage(named: imageName)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        var colorName = "Blue"
        if station?.status == false {
            colorName = "Red"
        }
        
        
        if selected {
            self.image = UIImage(named: "Station-Selected-\(colorName)")
        } else {
            self.image = UIImage(named: "Station-\(colorName)")
        }
        
    }
    
    
    
}
