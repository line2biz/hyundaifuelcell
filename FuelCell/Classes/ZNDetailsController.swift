//
//  ZNDetailsController.swift
//  FuelCell
//
//  Created by Шурик on 10.05.16.
//  Copyright © 2016 Alex. All rights reserved.
//

import UIKit
import CoreData
import CoreLocation
import MessageUI
import MapKit

struct DetailsItem {
    var title: String?
    private var imageName: String?
    private var identifier: String!
    private var completion: (() -> ())?
    
    static let GreyColor = UIColor.colorFromHex(0x515051, alpha: 1)
    static let BlueColor = UIColor.colorFromHex(0x00287a, alpha: 1)
    
    var image: UIImage? {
        get {
            if let name = imageName {
                return UIImage(named: name)
            } else {
                return nil
            }
        }
    }
    
    init(title: String, imageName: String, identifier: String) {
        self.title = title
        self.imageName = imageName
        self.identifier = identifier
    }
    
    
    
    
}

class ZNDetailsController: UITableViewController {
    
    //    MARK: - Types
    struct MainStoryboard {
        
        struct Colors {
            static let StationOn = UIColor.colorFromHex(0x0072cf)
            static let StationOff = UIColor.colorFromHex(0xf65959)
            static let DealerColor = UIColor.colorFromHex(0x00287a)
        }
        
        struct SegueIdentifiers {
            static let ShowDetailController = "ShowDetailController"
            static let ShowEmbedDetails = "ShowEmbedDetails"
        }
    }
    
//    MARK: Public properties
    var item: NSManagedObject? {
        didSet { configureView() }
    }
    
    var showCompleteInfo = false
    var userCoordinate: CLLocationCoordinate2D?
    
    lazy var distanceFormatter: MKDistanceFormatter = {
        let df = MKDistanceFormatter()
        df.unitStyle = .Default
        return df
    }()
    
    
//    MARK: Outlet properties
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var hoursLabel: UILabel!
    

//    MARK: Private properties
    private var repository = [DetailsItem]()
    
    
//    MARK: - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.clearsSelectionOnViewWillAppear = false
        
        // Table view configuration
        self.tableView.rowHeight = UITableViewAutomaticDimension;
        self.tableView.estimatedRowHeight = 57.0
        
//        if showCompleteInfo == false {
//            self.tableView.tableFooterView = nil
//        }

    }
    
    
//    MARK: - Public methods
    func height() -> CGFloat {
        
        var height: CGFloat = 0.0
        
        let count = tableView.numberOfRowsInSection(0)
        
        for idx in 0...count {
            let indexPath = NSIndexPath(forRow: idx, inSection: 0)
            height += CGRectGetHeight(tableView.rectForRowAtIndexPath(indexPath))
        }
        
        // Place holder for missing rows
        if count < 3 {
            height += CGFloat(abs(count - 3)) * tableView.estimatedRowHeight
        }
        
        if let view = tableView.tableHeaderView {
            height += CGRectGetHeight(view.frame)
        }
        
        if let view = tableView.tableFooterView {
            height += CGRectGetHeight(view.frame)
        }
        
        return height
    }
    
//    MARK: - Private methods
    private func configureView() {
        repository.removeAll()
        
        
        // Header panel
        if let station = self.item as? DMStation {
            tableView.tableHeaderView?.backgroundColor = station.status!.boolValue == true ? MainStoryboard.Colors.StationOn : MainStoryboard.Colors.StationOff
            titleLabel.text = station.owner
            hoursLabel.hidden = station.status!.boolValue
            
        } else {
            titleLabel.text = self.item?.valueForKey("name") as? String
            tableView.tableHeaderView?.backgroundColor = MainStoryboard.Colors.DealerColor
            hoursLabel.hidden = true
        }
        
        
        if let coordinate = self.userCoordinate, let station = self.item as? MKAnnotation  {
            let userLocation = CLLocation(latitude: coordinate.latitude , longitude: coordinate.longitude)
            let stationCoordinate = station.coordinate
            
            let stationLocation = CLLocation(latitude: stationCoordinate.latitude, longitude: stationCoordinate.longitude)
            let distance = userLocation.distanceFromLocation(stationLocation)
            
            distanceLabel.text = distanceFormatter.stringFromDistance(distance)
            
        } else {
            distanceLabel.text = ""
        }
        
        
        // Address
        let title: String = (self.item?.valueForKey("title")) as? String ?? ""
        let subtitle: String = self.item?.valueForKey("subtitle") as? String ?? ""
        let country: String = self.item?.valueForKey("country") as? String ?? ""
        let address = DetailsItem(title: "\(title)\n\(subtitle)\n\(country)", imageName: "Icon-Address-Blue", identifier: "")
        repository.append(address)
                
        
        // Phone
        if let phone = item?.valueForKey("phone") as? String {
            var item = DetailsItem(title: phone, imageName: "Icon-Phone-Blue", identifier: "")
            
            item.completion = {
                let phoneNumber: String = phone.stringByReplacingOccurrencesOfString(" ", withString: "").stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
                
                if let phoneCallURL:NSURL = NSURL(string:"tel://\(phoneNumber)") {
                    let application:UIApplication = UIApplication.sharedApplication()
                    if (application.canOpenURL(phoneCallURL)) {
                        application.openURL(phoneCallURL);
                    }
                }
            }
            
            repository.append(item)
            
        }
        
        // Email
        if let email = item?.valueForKey("email") as? String {
            
            let mail: String = email.stringByReplacingOccurrencesOfString(" ", withString: "").stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
            
            var item = DetailsItem(title: mail, imageName: "Icon-Email-Blue", identifier: "")
            item.completion = {
                if !MFMailComposeViewController.canSendMail() {
                    print("Mail services are not available")
                    return
                }
                
                let composeVC = MFMailComposeViewController()
                composeVC.mailComposeDelegate = self
                
                // Configure the fields of the interface.
                composeVC.setToRecipients([mail])
                //composeVC.setSubject("Hello!")
                //composeVC.setMessageBody("Hello from California!", isHTML: false)
                
                // Present the view controller modally.
                self.presentViewController(composeVC, animated: true, completion: nil)
            }
            repository.append(item)
        }
        
        // Working hours only for stations
        if let station = self.item as? DMStation {
            if let openTime = station.opening_time {
                let item = DetailsItem(title: openTime, imageName: "Icon-Clock-Blue", identifier: "")
                repository.append(item)
            }
        }
        
        
        
        tableView.reloadData()
    }
    
    
//    MARK: - Outlet methods
    @IBAction func didTapButtonNavigate(sender: AnyObject) {
        
        let station = self.item as! MKAnnotation
        let regionDistance:CLLocationDistance = 10000
        
        let regionSpan = MKCoordinateRegionMakeWithDistance(station.coordinate, regionDistance, regionDistance)
        let options = [
            MKLaunchOptionsMapCenterKey: NSValue(MKCoordinate: regionSpan.center),
            MKLaunchOptionsMapSpanKey: NSValue(MKCoordinateSpan: regionSpan.span)
        ]
        let placemark = MKPlacemark(coordinate: station.coordinate, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        
        
        if let st = self.item as? DMStation {
            mapItem.name = st.owner
            mapItem.phoneNumber = st.phone
            
        } else {
            mapItem.name = self.item?.valueForKey("name") as? String
            mapItem.phoneNumber = self.item?.valueForKey("phone") as? String
            
        }
        
        mapItem.openInMapsWithLaunchOptions(options)
    }
    
    // MARK: - Table view data source
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return repository.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! AZDetailTableCell
        let item = repository[indexPath.row]
        cell.iconImageView.image = item.image
        cell.titleLabel.text = item.title
        
        if let _ = item.completion {
            cell.titleLabel.textColor = DetailsItem.BlueColor
            cell.selectionStyle = .Default
            
        } else {
            cell.titleLabel.textColor = DetailsItem.GreyColor
            cell.selectionStyle = .None
        }
        
        return cell
    }
    
//    MARK: - Table View delegate
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if let completion = repository[indexPath.row].completion {
            completion()
        }
        let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(0.2 * Double(NSEC_PER_SEC)))
        dispatch_after(delayTime, dispatch_get_main_queue()) {
            self.tableView.deselectRowAtIndexPath(indexPath, animated: true)
        }
    }
    

}

//    MARK: - Mail Compose delegate
extension ZNDetailsController: MFMailComposeViewControllerDelegate {
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        controller.dismissViewControllerAnimated(true, completion: nil)
    }
}


