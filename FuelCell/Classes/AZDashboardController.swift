//
//  AZDashboardController.swift
//  FuelCell
//
//  Created by Шурик on 06.05.16.
//  Copyright © 2016 Alex. All rights reserved.
//

import UIKit

class AZDashboardController: UIViewController {
    
    @IBOutlet var stationButton: UIButton!
    @IBOutlet var dealerButton: UIButton!
    
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var subtitleLabel: UILabel!
    
    
//    MARK: View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Force portraint orientation
        let value = UIInterfaceOrientation.Portrait.rawValue
        UIDevice.currentDevice().setValue(value, forKey: "orientation")
        
        // Apppearance customization
        let imageView = UIImageView(image: UIImage(named: "Logo-Nav"))
        navigationItem.titleView = imageView
        
        stationButton.titleLabel?.numberOfLines = 2
        stationButton.titleLabel?.textAlignment = .Center
        dealerButton.titleLabel?.numberOfLines = 2
        dealerButton.titleLabel?.textAlignment = .Center
        
        var item: [String: String]
        var array: [AnyObject]
        if AppDelegate.sharedDelegate().phrasesItems.count > 0 {
            array = AppDelegate.sharedDelegate().phrasesItems
        } else {
            let url = NSBundle.mainBundle().URLForResource("Phrases", withExtension: "plist")!
            array = NSArray(contentsOfURL: url)! as [AnyObject]
        }
        
        let random = arc4random_uniform(UInt32(array.count - 1))
        item = array[(Int(random))] as! [String: String]
        
        
        titleLabel.text = item["title"]?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
        subtitleLabel.text = item["subtitle"]?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
        
        if UIDevice.currentDevice().userInterfaceIdiom == .Pad {
          
            titleLabel.font = titleLabel.font.fontWithSize(35)
            subtitleLabel.font = subtitleLabel.font.fontWithSize(40)
            
        }
        
        
    }
    
//    MARK: - Rotation
    override func shouldAutorotate() -> Bool {
        return false
    }
    
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.Portrait
    }
    
//    MARK: - Navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if let vc = segue.destinationViewController as? AZMapController {
            
            if let btn = sender as? UIButton {                
                if btn == stationButton {
                    vc.annotationType = .Station
                    vc.navigationItem.title = NSLocalizedString("Hydrogen Refuelling Stations", comment: "")
                } else {
                    vc.annotationType = .Dealer
                    vc.navigationItem.title = NSLocalizedString("Fuel Cell Dealers", comment: "")
                }
            }
            
        }
        
    }

}
