//
//  AZAnnotationsController.swift
//  FuelCell
//
//  Created by Шурик on 11.05.16.
//  Copyright © 2016 Alex. All rights reserved.
//

import UIKit
import CoreData
import MapKit
import SVProgressHUD


struct Annotation {
    var distance: CLLocationDistance
    var managedObject: NSManagedObject
    
    init(managedObject: NSManagedObject, distance: Double) {
        self.distance = distance
        self.managedObject = managedObject
    }
}

@objc protocol AZAnnotationsControllerDelegate : class {
    func annotationsController(annotationsController: AZAnnotationsController, didSelectObject object: NSManagedObject)
}

class AZAnnotationsController: UITableViewController {
    
//    MARK: - Public properties
    var managedObjectContext: NSManagedObjectContext? = DBManager.sharedManager.defaultContext
    var annotationType: AnnotationType = .Station    
    weak var delegate:AZAnnotationsControllerDelegate?
    
    var repository = Array<Annotation>()
    
    var userLocation: CLLocation? {
        didSet {
            reloadData()
        }
    }
    
    
    lazy var distanceFormatter: MKDistanceFormatter = {
        let df = MKDistanceFormatter()
        df.unitStyle = .Default
        return df
    }()

    
    
//
//    MARK: - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    

    
}

//    MARK: - Table View datasource
extension AZAnnotationsController {
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return repository.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! AZAnnotationTableCell
        
        let object = repository[indexPath.row]
        
        if let station = object.managedObject as? DMStation {
            cell.titleLabel.text = station.owner
            var arr = [String]()
            if let title = station.title {
                arr.append(title)
            }
            if let subtitle  = station.subtitle {
                arr.append(subtitle)
            }
            cell.iconImageView.image = UIImage(named: "Icon-Station")?.imageWithRenderingMode(.AlwaysTemplate)
            cell.subtitileLabel.text = arr.joinWithSeparator("\n")
            
        }
        else if let dealer = object.managedObject as? DMDealer {
            cell.titleLabel.text = dealer.name
            var arr = [String]()
            if let title = dealer.title {
                arr.append(title)
            }
            if let subtitle  = dealer.subtitle {
                arr.append(subtitle)
            }
            cell.subtitileLabel.text = arr.joinWithSeparator("\n")
            cell.iconImageView.image = UIImage(named: "Hyundai-Logo-Small")
        }
        
        cell.distanceLabel.text = distanceFormatter.stringFromDistance(object.distance)
        
        return cell
    }
    
    
    func reloadData()  {
        
        repository.removeAll()
        defer {
           self.tableView.reloadData()
        }
        
        do {
            if let userLocation = self.userLocation {
                
                let fetchRequest = NSFetchRequest()
                // Edit the entity name as appropriate.
                var entity: NSEntityDescription
                
                switch self.annotationType {
                case .Station:
                    entity  = NSEntityDescription.entityForName("DMStation", inManagedObjectContext: self.managedObjectContext!)!
                case .Dealer:
                    entity = NSEntityDescription.entityForName("DMDealer", inManagedObjectContext: self.managedObjectContext!)!
                }
                fetchRequest.entity = entity
                
                let results = try managedObjectContext?.executeFetchRequest(fetchRequest) as! [MKAnnotation]
                for obj in results {
                    if CLLocationCoordinate2DIsValid(obj.coordinate) {
                        let objLocation = CLLocation(latitude: obj.coordinate.latitude, longitude: obj.coordinate.longitude)
                        let distance = userLocation.distanceFromLocation(objLocation)
                        repository.append(Annotation(managedObject: (obj as! NSManagedObject), distance: distance))
                    }
                }
                
            }
            
            repository.sortInPlace { (obj1, obj2) -> Bool in
                return obj1.distance < obj2.distance
            }
            
        }
        catch let error as NSError {
            print("\(#function) error: \(error)")
        }
        
        
        
        
        
    }
}

//    MARK: - Table View delegate
extension AZAnnotationsController {
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let obj = repository[indexPath.row]
        
        delegate?.annotationsController(self, didSelectObject: obj.managedObject)
    }
}

//    MARK: - Fetched Results controller delegate
extension AZAnnotationsController: NSFetchedResultsControllerDelegate  {
    func controllerDidChangeContent(controller: NSFetchedResultsController) {
//        reloadData()
    }
}
