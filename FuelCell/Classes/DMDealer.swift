//
//  DMDealer.swift
//  FuelCell
//
//  Created by Шурик on 12.05.16.
//  Copyright © 2016 Alex. All rights reserved.
//

import Foundation
import CoreData
import MapKit
import Gloss

struct TempStruct {
    var number: Int
    var long: Double
    var lat: Double
    
    init(number: Int, coordinate: CLLocationCoordinate2D) {
        self.number = number
        self.long = coordinate.longitude
        self.lat = coordinate.latitude
    }
    
}


class DMDealer: NSManagedObject {
    
    struct Keys {
        static let DealerID = "dealerID"
    }

    class func dealer(dealerID: NSNumber, inManagedObjectContext context: NSManagedObjectContext) -> DMDealer? {
        
        let fetchRequest = NSFetchRequest(entityName: self.nameOfClass)
        fetchRequest.predicate = NSPredicate(format: "dealerID = %@", dealerID)
        fetchRequest.fetchLimit = 1
        do {
            let resullts = try context.executeFetchRequest(fetchRequest) as? [DMDealer]
            return resullts!.first
        }
        catch let error as NSError {
            print("\(self.nameOfClass):\(#function) error: \(error)")
        }
        return nil
    }
    
    class func new(dealerID: NSNumber, inManagedObjectContext context: NSManagedObjectContext) -> DMDealer {
        print("\(String(self)):\(#function)")
        let entity = NSEntityDescription.insertNewObjectForEntityForName(String(DMDealer), inManagedObjectContext: context) as! DMDealer
        entity.dealerID = dealerID
        return entity
    }
    
    
    class func parseResponse(response: AnyObject, inManagedObjectContext context: NSManagedObjectContext) {
        
        guard let dict = response as? NSDictionary else {
            print("\(String(self)):\(#function) cannot parse json")
            return
        }
        
        guard let entities = dict.allValues as? [JSON] else {
            print("\(String(self)):\(#function) cannot parse values")
            return
        }
        
        let allKeys = dict.allKeys
        
        
        let fr = NSFetchRequest(entityName: String(DMDealer))
        
        // Remove old
        fr.predicate = NSPredicate(format: "NOT (%K in %@)", DMDealer.Keys.DealerID, allKeys)
        let itemsToRemove = try! context.executeFetchRequest(fr) as! [DMDealer]
        for item in itemsToRemove {
            context.deleteObject(item)
        }
        
        // Select existing
        fr.predicate = NSPredicate(format: "ANY %K in %@", DMDealer.Keys.DealerID, allKeys)
        let results = try! context.executeFetchRequest(fr) as! [DMDealer]
        let keys: [NSNumber] = results.map({ $0.dealerID! })
        var dealers = NSDictionary(objects: results, forKeys: keys) as! [NSNumber: DMDealer]
        
        print((entities.first! as NSDictionary))
        
        for json in entities {
            guard let sID: NSString? = "id" <~~ json else {
                print("\(String(self)):\(#function) cannot find ID")
                continue
            }
            
            let guid = NSNumber(integer: sID!.integerValue)
            let dealer = dealers[guid] ?? DMDealer.new(guid, inManagedObjectContext: context)
            
            dealer.street = "address" <~~ json
            dealer.city = "city" <~~ json
            dealer.country = "country" <~~ json
            dealer.responsible_person = "responsible_person" <~~ json
            dealer.countryCode = "iso_code" <~~ json
            dealer.email = "email" <~~ json
            dealer.phone = "phone" <~~ json
            dealer.name = "name" <~~ json
            
//            dealer.longitude = "lng"  <~~ json
//            dealer.latitude = "lat" <~~ json
            
            let lat: NSString? = "lat" <~~ json
            dealer.latitude = lat?.doubleValue
            
            let lng: NSString? = "lng" <~~ json
            dealer.longitude = lng?.doubleValue
            
            
            dealer.zip = "zip" <~~ json
            dealer.mobile = "mobile" <~~ json

            do {
                if dealer.inserted {
                    try dealer.validateForInsert()
                } else {
                    try dealer.validateForUpdate()
                }
            }
            catch let error as NSError {
                print("\(String(self)):\(#function) error: \(error.userInfo)")
                context.deleteObject(dealer)
            }
            
        }
        
        if context.hasChanges {
            try! context.save()
        }

        
//        if let entities = response as? NSDictionary {
//            var serverSet = Set<DMDealer>()
//            for (key, value) in entities {
//                let sID = (key as! NSString).integerValue  // key as! NSNumber
//                
//                
//                
//                if let json = value as? JSON {
//                    var dealer: DMDealer? = DMDealer.dealer(NSNumber(integer:sID), inManagedObjectContext: context)
//                    if dealer == nil {
//                        dealer = NSEntityDescription.insertNewObjectForEntityForName("DMDealer", inManagedObjectContext: context) as? DMDealer
//                        dealer?.dealerID = "id" <~~ json
//                    }
//                    
//                    serverSet.insert(dealer!)
//
//                    dealer?.street = "address" <~~ json
//                    dealer?.city = "city" <~~ json
//                    
//                    
//                    dealer?.country = "country" <~~ json
//                    
//                    dealer?.responsible_person = "responsible_person" <~~ json
//                    
//                    
//                    dealer?.countryCode = "iso_code" <~~ json
//                    dealer?.email = "email" <~~ json
//                    dealer?.phone = "phone" <~~ json
//                    dealer?.name = "name" <~~ json
//                    dealer?.longitude = "lng"  <~~ json
//                    dealer?.latitude = "lat" <~~ json
//                    dealer?.zip = "zip" <~~ json
//                    dealer?.mobile = "mobile" <~~ json
//            
//                }
//            }
//            // Remover local entities if they are missing in the server side
//            let fetchRequest = NSFetchRequest(entityName: "DMDealer")
//            let results = try! context.executeFetchRequest(fetchRequest)
//            let localSet = NSMutableSet(array: results)
//            localSet.minusSet(serverSet)
//            
//            if localSet.count > 0 {
//                for obj in localSet {
//                    context.deleteObject(obj as! NSManagedObject)
//                }
//            }
//        }
                //            DMDealer.findEmptyDealers(serverSet)
        
    }

    
    
    
    
    /*
     Temporary functions to find coordinate for the dealers
     */
    class func findEmptyDealers(serverSet: Set<DMDealer>) {
        let serviceGroup: dispatch_group_t = dispatch_group_create()
        var array = Array<TempStruct>()
        
        
        for dealer in serverSet {
            // Get location coordinates
            
            
            if CLLocationCoordinate2DIsValid(dealer.coordinate) == false || dealer.city == nil {
                
                dispatch_group_enter(serviceGroup)
                
                var arr = [String]()
                if let val = dealer.street         { arr.append(val) }
                if let val = dealer.zip            { arr.append(val) }
                if let val = dealer.city           { arr.append(val) }
                if let val = dealer.countryCode    { arr.append(val) }
                
                let geocoder = CLGeocoder()
                geocoder.geocodeAddressString(arr.joinWithSeparator(", "), completionHandler: { (placemarks, error) in
                    if let placemark = placemarks?.first {
                        if CLLocationCoordinate2DIsValid(dealer.coordinate) == false {
                            let coordinate = (placemark.location?.coordinate)!
                            
                            dealer.latitude = coordinate.latitude
                            dealer.longitude = coordinate.longitude
                            
                            let tempItem = TempStruct(number: dealer.dealerID!.integerValue, coordinate: (placemark.location?.coordinate)!)
                            array.append(tempItem)
                            
                        }
                        
                        // Location name
                        if let locationName = placemark.addressDictionary!["City"] as? String where dealer.city == nil {
                            dealer.city = locationName
                            
                            //                                print("\(locationName)")
                            
                        }
                        
                        
                        
                    }
                    
                    dispatch_group_leave(serviceGroup)
                })
            }
        }
        
        
        dispatch_group_notify(serviceGroup, dispatch_get_main_queue(), {
            array.sortInPlace({ (obj1, obj2) -> Bool in
                obj1.number < obj2.number
            })
            for item in array {
                print("\(item.number)\tlat: \(item.lat)\tlon: \(item.long)")
            }
            
        
        })
    
        


    }
    
}


//    MARK: - MKAnnotation Protocol
extension DMDealer: MKAnnotation {
    
    var coordinate: CLLocationCoordinate2D {
        get {
            if let lat = self.latitude, let lon = self.longitude {
                return CLLocationCoordinate2DMake(CLLocationDegrees(lat), CLLocationDegrees(lon))
            } else {
                return kCLLocationCoordinate2DInvalid
            }
        }
    }
    
    // Title and subtitle for use by selection UI.
    var title: String? { get { return self.street } }
    var subtitle: String? {
        get {
            
            var arr = [String]()
            if let zip = self.zip {
                arr.append(zip)
            }
            
            if let city = self.city {
                arr.append(city)
            }
            return arr.joinWithSeparator(" ") ?? ""
        }
    }
}
