
//
//  AZDealerAnnotationView.swift
//  FuelCell
//
//  Created by Шурик on 16.05.16.
//  Copyright © 2016 Alex. All rights reserved.
//

import UIKit
import MapKit

class AZDealerAnnotationView: MKAnnotationView {

    override init(annotation: MKAnnotation?, reuseIdentifier: String?) {
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier)
        commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    private func commonInit() {
        self.image = UIImage(named: "Dealer-Blue")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if selected {
            self.image = UIImage(named: "Dealer-Selected-Blue")
        } else {
            self.image = UIImage(named: "Dealer-Blue")
        }
        
    }
}
