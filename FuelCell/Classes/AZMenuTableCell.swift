//
//  AZMenuTableCell.swift
//  FuelCell
//
//  Created by Шурик on 07.05.16.
//  Copyright © 2016 Alex. All rights reserved.
//

import UIKit

class AZMenuTableCell: UITableViewCell {

    @IBOutlet weak var titleMenu: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        selectedBackgroundView = UIView()
        selectedBackgroundView?.backgroundColor = UIColor.mainAppColor()
        
        backgroundColor = UIColor.clearColor()
        contentView.backgroundColor = UIColor.clearColor()
        
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
