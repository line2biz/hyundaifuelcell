//
//  AZMenuPresentationController.swift
//  FuelCell
//
//  Created by Шурик on 07.05.16.
//  Copyright © 2016 Alex. All rights reserved.
//

import UIKit

class AZMenuPresentationController: UIPresentationController {
    
//    MARK: - Settings
    let backgroundAlpha: CGFloat = 0.75
    
//    MARK: - Properties
    private let backgroundView = UIView()
    private let dimmingView = UIView()
    private var panStartPoint: CGPoint = CGPointZero
    
    

//    MARK: - Initialization
    override init(presentedViewController: UIViewController, presentingViewController: UIViewController) {
        super.init(presentedViewController: presentedViewController, presentingViewController: presentingViewController)
    
        // Prepare bacground view
        backgroundView.backgroundColor = UIColor.redColor().colorWithAlphaComponent(0)
        
        
        // Prepare dimming view
        dimmingView.backgroundColor = UIColor(white: 0, alpha: 0.75)
        dimmingView.alpha = 0.0
        
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(AZMenuPresentationController.handleTapRecognizer(_:)))
        backgroundView.addGestureRecognizer(tapRecognizer)
        
        let panRecognizer = UIPanGestureRecognizer(target: self, action: #selector(AZMenuPresentationController.handlePanRecognizer(_:)))
        backgroundView.addGestureRecognizer(panRecognizer)
    }
    
//    MARK: - Override methods
    override func presentationTransitionWillBegin() {
        let containerView = self.containerView!
        
        // Allow pan recognizer
//        presentedViewController.view.userInteractionEnabled = false
        
        // Add clear background
        backgroundView.frame = backgroundView.bounds
        containerView.insertSubview(backgroundView, atIndex: 0)
        
        
        // Dimming view
        dimmingView.frame = containerView.frame
        dimmingView.frame.origin.y = heightOfNavBar()
        dimmingView.frame.size.height -= heightOfNavBar()
        dimmingView.alpha = 0
        backgroundView.addSubview(dimmingView)
        
        if let coordinator = presentedViewController.transitionCoordinator() {
            coordinator.animateAlongsideTransition({ (_) in
                self.dimmingView.alpha = 1
                }, completion: nil)
        } else {
            dimmingView.alpha = 1
        }
        
    }
    
    override func dismissalTransitionWillBegin() {
        if let coordinator = presentedViewController.transitionCoordinator() {
            coordinator.animateAlongsideTransition({ (_) in
                self.dimmingView.alpha = 0
                }, completion: nil)
        } else {
            dimmingView.alpha = 0
        }
    }
    
    override func sizeForChildContentContainer(container: UIContentContainer, withParentContainerSize parentSize: CGSize) -> CGSize {
        return CGSizeMake(280, parentSize.height - heightOfNavBar())
//        return self.containerSize
    }
    
    override func containerViewWillLayoutSubviews() {
        
        backgroundView.frame = (self.containerView?.bounds)!
        
        dimmingView.frame = backgroundView.bounds
        dimmingView.frame.origin.y = heightOfNavBar()
        dimmingView.frame.size.height -= heightOfNavBar()
        
        presentedView()?.frame = frameOfPresentedViewInContainerView()
        
        heightOfNavBar()
    }
    
    override func frameOfPresentedViewInContainerView() -> CGRect {
        var presentedViewFrame = CGRectZero
        presentedViewFrame.origin.y = heightOfNavBar()
        let containerBounds = containerView!.bounds
        presentedViewFrame.size = sizeForChildContentContainer(presentedViewController, withParentContainerSize: containerBounds.size)
        return presentedViewFrame
    }
    
//    MARK: - Outlet methods
    func  handleTapRecognizer(recognizer: UITapGestureRecognizer) {
        if recognizer.state == .Ended {
            presentingViewController.dismissViewControllerAnimated(true, completion: nil)
        }
    }
    
    func  handlePanRecognizer(recognizer: UIPanGestureRecognizer) {
    
        switch recognizer.state {
        case .Changed:
            let viewToMove = presentedViewController.view
            let translation = recognizer.translationInView(recognizer.view)
            
            if (viewToMove.center.x + translation.x) <= CGRectGetMidX(viewToMove.bounds) {
                viewToMove.center.x += translation.x
            }
            recognizer.setTranslation(CGPointZero, inView: recognizer.view)
            
            
        case .Ended, .Cancelled:
            let viewToMove = presentedViewController.view
            if fabs(CGRectGetMinX(viewToMove.frame)) > CGRectGetWidth(viewToMove.frame)/2 {
                presentingViewController.dismissViewControllerAnimated(true, completion: nil)
                
                
            } else {
                UIView.animateWithDuration(0.25, delay:0, usingSpringWithDamping:300.0, initialSpringVelocity:5.0, options:UIViewAnimationOptions.AllowUserInteraction, animations:{
                    
                    viewToMove.center.x = CGRectGetMidX(viewToMove.bounds)
                    
                    }, completion:{ (value: Bool) in
                        
                })
            }
            break
        default: return
        }
        
    }
    
//    MARK: - Private methods
    private func heightOfNavBar() -> CGFloat {
        if let navController = presentingViewController as? UINavigationController {
            return CGRectGetMaxY(navController.navigationBar.frame)
        }
        return 0
    }
    
}

extension AZMenuPresentationController: UIAdaptivePresentationControllerDelegate {
    override func shouldPresentInFullscreen() -> Bool {
        return true
    }
    
    override func adaptivePresentationStyle() -> UIModalPresentationStyle {
        return .FullScreen
    }
}
