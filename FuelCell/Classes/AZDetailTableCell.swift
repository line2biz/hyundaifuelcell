//
//  AZDetailTableCell.swift
//  FuelCell
//
//  Created by Шурик on 10.05.16.
//  Copyright © 2016 Alex. All rights reserved.
//

import UIKit


class AZDetailTableCell: UITableViewCell {
    
//    MARK: - Outlet properties
    
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
