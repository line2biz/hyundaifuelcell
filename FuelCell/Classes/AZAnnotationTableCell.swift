//
//  AZMapTableCell.swift
//  FuelCell
//
//  Created by Шурик on 06.05.16.
//  Copyright © 2016 Alex. All rights reserved.
//

import UIKit

class AZAnnotationTableCell: UITableViewCell {
    
//    MARK: - Outlets
    
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitileLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        let view = UIView()
        view.backgroundColor = UIColor.mainAppColor()
        selectedBackgroundView = view
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
