//
//  AZBrowserController.swift
//  FuelCell
//
//  Created by Шурик on 11.05.16.
//  Copyright © 2016 Alex. All rights reserved.
//

import UIKit
import WebKit

class AZBrowserController: UIViewController {
    
//    Public properties
    var openURL: NSURL?
    var webView = WKWebView()
    
//    MARK: - Outlets
    @IBOutlet var progressView: UIProgressView!
    

//    MARK: - View Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        progressView.progress = 0.0
        
        // Customize web view
        webView.navigationDelegate = self
        webView.frame = self.view.bounds
        webView.allowsBackForwardNavigationGestures = true
        webView.autoresizingMask = [.FlexibleHeight, .FlexibleWidth]
        self.view.insertSubview(webView, atIndex: 0)
        
        if let _ = openURL {
            let request = NSURLRequest(URL: openURL!)
            webView.loadRequest(request)
        }
        
        registerObservers()
    }
    
    deinit {
        unregisterObservers()
    }
    
//    MARK: - Observer
    func registerObservers() {
        webView.addObserver(self, forKeyPath: "estimatedProgress", options: .New, context: nil)
    }
    
    func unregisterObservers() {
        webView.removeObserver(self, forKeyPath: "estimatedProgress")
    }
    
    override func observeValueForKeyPath(keyPath: String?, ofObject object: AnyObject?, change: [String : AnyObject]?, context: UnsafeMutablePointer<Void>) {
        if keyPath == "estimatedProgress" {
            self.progressView.hidden = self.webView.estimatedProgress == 1
            self.progressView.progress = Float(webView.estimatedProgress)
        } else {
            return super.observeValueForKeyPath(keyPath, ofObject: object, change: change, context: context)
        }
    }
    
    
}

extension AZBrowserController: WKNavigationDelegate {
    func webView(webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        progressView.hidden = false
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
    }
    
    func webView(webView: WKWebView, didFinishNavigation navigation: WKNavigation!) {
        progressView.hidden = true
        progressView.progress = 0.0
        UIApplication.sharedApplication().networkActivityIndicatorVisible = false
    }
    
    func webView(webView: WKWebView, didFailNavigation navigation: WKNavigation!, withError error: NSError) {
        UIApplication.sharedApplication().networkActivityIndicatorVisible = false
    }
}
