//
//  UIColor+Castomization.swift
//  FuelCell
//
//  Created by Шурик on 04.05.16.
//  Copyright © 2016 Alex. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    
    
    class func colorFromHex(rgbValue:UInt32, alpha:Double=1.0)->UIColor {
        let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
        let blue = CGFloat(rgbValue & 0xFF)/256.0
        
        return UIColor(red:red, green:green, blue:blue, alpha:CGFloat(alpha))
    }
    
    class func mainAppColor()->UIColor {        
        return UIColor.colorFromHex(0x00246f, alpha: 1)
    }
}
