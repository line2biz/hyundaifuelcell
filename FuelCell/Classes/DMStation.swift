//
//  DMStation.swift
//  FuelCell
//
//  Created by Шурик on 07.05.16.
//  Copyright © 2016 Alex. All rights reserved.
//

import Foundation
import CoreData
import MapKit
import Gloss

class DMStation: NSManagedObject {
    
    struct Keys {
        static let StationID = "stationID"
    }
    

//    MARK: - Class functions
    class func station(stationID: NSNumber, inManagedObjectContext context: NSManagedObjectContext) -> DMStation? {
        
        let fetchRequest = NSFetchRequest(entityName: self.nameOfClass)
        fetchRequest.predicate = NSPredicate(format: "stationID = %@", stationID)
        fetchRequest.fetchLimit = 1
        do {
            let resullts = try context.executeFetchRequest(fetchRequest) as? [DMStation]
            return resullts!.first
        }
        catch let error as NSError {
            print("\(self.nameOfClass):\(#function) error: \(error)")
        }
        return nil
    }
    
    /**
     Creates a new station with given ID
    */
    class func new(stationID: NSNumber, inManagedObjectContext context: NSManagedObjectContext) -> DMStation {
        let entity = NSEntityDescription.insertNewObjectForEntityForName(String(DMStation), inManagedObjectContext: context) as! DMStation
        entity.stationID = stationID
        return entity
    }
    
    
    class func parseResponse(response: AnyObject, inManagedObjectContext context: NSManagedObjectContext) {
        
        
        
        
        let nestedContext = NSManagedObjectContext(concurrencyType: .PrivateQueueConcurrencyType)
        nestedContext.parentContext = context
        nestedContext.performBlockAndWait { 
            if let dict = response as? NSDictionary {
                
                let allKeys = dict.allKeys
                let fr = NSFetchRequest(entityName: String(DMStation))
                
                // Remove old
                fr.predicate = NSPredicate(format: "NOT (%K in %@)", DMStation.Keys.StationID, allKeys)
                let itemsToRemove = try! nestedContext.executeFetchRequest(fr) as! [DMStation]
                for item in itemsToRemove {
                    nestedContext.deleteObject(item)
                }
                
                // Select existing
                fr.predicate = NSPredicate(format: "ANY %K in %@", DMStation.Keys.StationID, allKeys)
                let results = try! nestedContext.executeFetchRequest(fr) as! [DMStation]
                let keys: [NSNumber] = results.map({ $0.stationID! })
                var stations = NSDictionary(objects: results, forKeys: keys) as! [NSNumber: DMStation]
                
                
                
                // parse
                guard let entities = dict.allValues as? [JSON] else {
                    print("\(String(self)):\(#function) cannot parse JSON")
                    return
                }
                
                for json in entities {
                    
                    guard let tID: NSString = "id" <~~ json else {
                        print("\(String(self)):\(#function) ID not found")
                        continue
                    }
                    
                    let sID = NSNumber(integer: tID.integerValue)
                    
                    
                    let station = stations[sID] ?? DMStation.new(sID, inManagedObjectContext: nestedContext)
                    station.updated_at = "updated_at" <~~ json
                    station.country = "country" <~~ json
                    station.street = "address" <~~ json
                    station.city = "city" <~~ json
                    station.countryCode = "iso_code" <~~ json
                    station.email = "email" <~~ json
                    
                    let lat: NSString? = "lat" <~~ json
                    station.latitude = lat?.doubleValue
                    
                    let lng: NSString? = "lng" <~~ json
                    station.longitude = lng?.doubleValue
                    
                    station.opening_time = "opening_time" <~~ json
                    station.owner = "owner" <~~ json
                    station.phone = "phone" <~~ json
                    station.provider = "provider" <~~ json
                    station.responsible_person = "responsible_person" <~~ json
                    station.zip = "zip" <~~ json
                    
                    let status: String? = "status" <~~ json
                    station.status = status == "OPEN" ? true : false
                    
                    do {
                        if station.inserted {
                            try station.validateForInsert()
                        } else {
                            try station.validateForUpdate()
                        }
                    }
                    catch let error as NSError {
                        print("\(#function) error: \(error.userInfo)")
                        nestedContext.deleteObject(station)
                    }
                    
                }
                
                do {
                    try nestedContext.save()
                }
                catch let error as NSError {
                    print("\(#function) error: \(error)")
                }
                DBManager.sharedManager.saveContext()
            }
            
        }
    }
}


//    MARK: - MKAnnotation Protocol
extension DMStation: MKAnnotation {
    
    var coordinate: CLLocationCoordinate2D {
        get {
            if let lat = self.latitude, let lon = self.longitude {
                return CLLocationCoordinate2DMake(CLLocationDegrees(lat), CLLocationDegrees(lon))
            } else {
                return kCLLocationCoordinate2DInvalid
            }
        }
    }
    
    // Title and subtitle for use by selection UI.
    var title: String? { get { return self.street } }
    var subtitle: String? {
        get {
            var arr = [String]()
            if let zip = self.zip {
                arr.append(zip)
            }
            
            if let city = self.city {
                arr.append(city)
            }
            
            return arr.joinWithSeparator(" ") ?? ""
        }
    }
}
