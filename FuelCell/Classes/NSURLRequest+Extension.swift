//
//  NSURLRequest+Extension.swift
//  FuelCell
//
//  Created by Шурик on 12.05.16.
//  Copyright © 2016 Alex. All rights reserved.
//

import UIKit

public extension NSURLRequest {
    
    public class func authorithedRequest(url: NSURL) -> NSMutableURLRequest {
        
        let request = NSMutableURLRequest(URL: url)
        let userPasswordString = "lvuser:aF96uGmx"
        let userPasswordData = userPasswordString.dataUsingEncoding(NSUTF8StringEncoding)
        let base64EncodedCredential = userPasswordData!.base64EncodedStringWithOptions([])
        let authString = "Basic \(base64EncodedCredential)"
        request.addValue(authString, forHTTPHeaderField: "Authorization")
        return request

        
        
    }
    
}
