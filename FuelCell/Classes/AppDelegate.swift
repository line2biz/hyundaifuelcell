//
//  AppDelegate.swift
//  FuelCell
//
//  Created by Шурик on 04.05.16.
//  Copyright © 2016 Alex. All rights reserved.
//

import UIKit
import CoreData
import CoreLocation
import MapKit
import SVProgressHUD
import Gloss

public enum AnnotationType {
    case Station
    case Dealer
}


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UISplitViewControllerDelegate {

    var window: UIWindow?
    let locationManager = CLLocationManager()
    var userLocation: CLLocation?
    
    var phrasesItems = [[String: String]]()
    

//    MARK: - Application Delegate
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {

        customizeAppearance()
        

        
        let status = CLLocationManager.authorizationStatus()
        if status == .NotDetermined || status == .Denied || status == .AuthorizedWhenInUse {
            locationManager.requestAlwaysAuthorization()
            locationManager.requestWhenInUseAuthorization()
        }
        
        
        // Load phrases for dashboard
        loadDataFromServer()
        
        
        return true
    }
    
    func loadDataFromServer() {
        
        
        let url = APIController.URL(forPath: "get-notices")
        
        let configuration = NSURLSessionConfiguration.defaultSessionConfiguration()
        let session = NSURLSession(configuration: configuration)
        let request = NSURLRequest.authorithedRequest(url)
        
        //        let request = NSURLRequest(URL: url)
        let semaphore = dispatch_semaphore_create(0)
        let task = session.dataTaskWithRequest(request) { (data, response, error) in
            
            if let _ = error {
                print("\(#function) error: \(error)")
                dispatch_semaphore_signal(semaphore)
            } else {
                
                do {
                    
                    if let json = try NSJSONSerialization.JSONObjectWithData(data!, options: .AllowFragments) as? NSDictionary {
                        for (_, value) in json {
                            let item = [
                                "title": value["title"] as! String,
                                "subtitle": value["body"] as! String
                            ]
                            self.phrasesItems.append(item)
                        }
                    }
                    
                    
                    dispatch_semaphore_signal(semaphore)
                }
                catch let error as NSError {
                    print("\(#function) error: \(error)")
                    dispatch_semaphore_signal(semaphore)
                }
                
            }
        }
        task.resume()
        
        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER)
        
        
    }
    
    
    func applicationWillTerminate(application: UIApplication) {
        DBManager.sharedManager.saveContext()
    }
    
    func applicationDidEnterBackground(application: UIApplication) {
        DBManager.sharedManager.saveContext()
    }
    
//    MARK: - Restoration
    func application(application: UIApplication, shouldSaveApplicationState coder: NSCoder) -> Bool {
        return false
    }
    
    func application(application: UIApplication, shouldRestoreApplicationState coder: NSCoder) -> Bool {
        return false
    }
    
//    MARK: - Public functins
    class func sharedDelegate() -> AppDelegate {
        return UIApplication.sharedApplication().delegate as! AppDelegate
    }

//    MARK: - Appearance
    func customizeAppearance() {
        
        self.window?.tintColor = UIColor.mainAppColor()
        
        // Navigation bar
        let navBar = UINavigationBar.appearance()
        navBar.barTintColor = UIColor.mainAppColor()
        navBar.tintColor = UIColor.whiteColor()
        navBar.titleTextAttributes = [ NSForegroundColorAttributeName: UIColor.whiteColor(), NSFontAttributeName: UIFont(name: "ModernH-EcoLight", size: 20)!]
        
        
        // SVProgressHUD
        let appearance = SVProgressHUD.appearance()
        appearance.defaultStyle = SVProgressHUDStyle.Custom
        appearance.minimumDismissTimeInterval = 0.5
        appearance.foregroundColor = UIColor.whiteColor()
        appearance.backgroundColor = UIColor.mainAppColor()
        
        
    }
    

}

