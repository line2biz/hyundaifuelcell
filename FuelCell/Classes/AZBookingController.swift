//
//  AZBookingController.swift
//  FuelCell
//
//  Created by Шурик on 16.05.16.
//  Copyright © 2016 Alex. All rights reserved.
//

import UIKit
import SVProgressHUD

class AZBookingController: UITableViewController {

    @IBOutlet var firstNameTestField: UITextField!
    @IBOutlet var lastNameTestField: UITextField!
    @IBOutlet var emailTextField: UITextField!
    @IBOutlet var phoneTestField: UITextField!
    @IBOutlet var landTestField: UITextField!
    @IBOutlet var cityTestField: UITextField!
    @IBOutlet var zipTestField: UITextField!
    @IBOutlet var messageTextView: UITextView!
    
    @IBOutlet var sendButton: UIButton!
    
    @IBOutlet var termsLabel: UILabel!
    
//    MARK: View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: NSLocalizedString("Back", comment: ""), style: .Plain, target: nil, action: nil)

        //  Underline the text
        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: termsLabel.text!)
        attributeString.addAttribute(NSUnderlineStyleAttributeName, value: 1, range: NSMakeRange(0, attributeString.length))
        termsLabel.attributedText = attributeString
    }
    
//    MARK: - Rotation
    override func shouldAutorotate() -> Bool {
        return false
    }
    
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.Portrait
    }
    
//    MARK: Private methods
    func showAlert(msg: String, completion: (() -> ())) {
        let alertController = UIAlertController(title: self.title, message: msg, preferredStyle: .Alert)
        alertController.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .Cancel, handler: { (alertAction) in
            completion()
        }))
        presentViewController(alertController, animated: true, completion: nil)
    }
    
//    MARK: - Outlet methods
    @IBAction func didTapButtonSend(sender: AnyObject) {
        
        
        if firstNameTestField.hasText() == false {
            showAlert(NSLocalizedString("'First name' should be filled in!", comment: ""), completion: {
                self.firstNameTestField.becomeFirstResponder()
            })
            return
        }
        
        if lastNameTestField.hasText() == false {
            showAlert(NSLocalizedString("'Last name' should be filled in!", comment: ""), completion: {
                self.lastNameTestField.becomeFirstResponder()
            })
            return
        }
        
        if emailTextField.hasText() == false && phoneTestField.hasText() == false {
            showAlert(NSLocalizedString("'Phone' or 'Email' should be filled in!", comment: ""), completion: {
                self.emailTextField.becomeFirstResponder()
            })
            return
        }
        
        if emailTextField.text?.isValidEmail == false {
            showAlert(NSLocalizedString("'Email' isn't valid!", comment: ""), completion: {
                self.emailTextField.becomeFirstResponder()
            })
            return
        }
        
        if messageTextView.hasText() == false {
            showAlert(NSLocalizedString("'Message' should be filled in!", comment: ""), completion: {
                self.lastNameTestField.becomeFirstResponder()
            })
            return
        }
//

//        let request = NSMutableURLRequest(re)
        
        let msg = messageTextView.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet()).stringByReplacingOccurrencesOfString("\n", withString: " ")
        
        let values = [
            "first_name": firstNameTestField.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet()),
            "last_name": lastNameTestField.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet()),
            "email": emailTextField.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet()),
            "phone": phoneTestField.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet()),
            "land": landTestField.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet()),
            "city": cityTestField.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet()),
            "zip": zipTestField.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet()),
            "msg": msg

        ]
        
        var array = [NSURLQueryItem]()
        for (key, value) in values {
            array.append(NSURLQueryItem(name: key, value: value))
        }
        
        let url = APIController.URL(forPath: "send-test-drive", queryItems: array)
        
        SVProgressHUD.show()
        
        let configuration = NSURLSessionConfiguration.defaultSessionConfiguration()
        let session = NSURLSession(configuration: configuration)
        
        let task = session.dataTaskWithRequest(NSURLRequest.authorithedRequest(url)) { (data, response, error) in
            if let _ = error {
                SVProgressHUD.showErrorWithStatus(error!.localizedDescription)
                
            } else {
//                let str = NSString(data: data!, encoding: NSUTF8StringEncoding)
//                print("asdfasdf \(str)")
                
                if let httpResponse = response as? NSHTTPURLResponse {
                    if httpResponse.statusCode == 200 {
                        SVProgressHUD.showSuccessWithStatus(NSLocalizedString("Sent", comment: ""))
                    } else {
                        SVProgressHUD.showErrorWithStatus(NSLocalizedString("Message hasn't been sent", comment: ""))
                    }
                }
            }
        }
        task.resume()
        
        
//        let url = NSURL(string: "http://hyundai.devinstance.de/send-test-drive")!
//        let request = NSURLRequest.authorithedRequest(url)
        
       
    }
    
    @IBAction func switchValueChanged(sender: UISwitch) {
        sendButton.enabled = sender.on
    }
    
//    MARK: - Table View delegate
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if indexPath.row == tableView.numberOfRowsInSection(0) - 1 {
            
            let style: UIAlertControllerStyle = UIDevice.currentDevice().userInterfaceIdiom == .Pad ? .Alert : .ActionSheet
            let alertController = UIAlertController(title: nil, message: nil, preferredStyle: style)
            alertController.addAction(UIAlertAction(title:  NSLocalizedString("Terms and Conditions", comment: ""), style: .Default, handler: { (alertAction) in
                if let vc = self.storyboard?.instantiateViewControllerWithIdentifier("AZBrowserController") as? AZBrowserController {
                    vc.openURL = NSBundle.mainBundle().URLForResource("Terms", withExtension: "html")
                    vc.navigationItem.title = NSLocalizedString("Terms and Conditions", comment: "")
                    vc.navigationItem.leftBarButtonItem = nil
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }))
            alertController.addAction(UIAlertAction(title: NSLocalizedString("Privacy policy", comment: ""), style: .Default, handler: { (alertAction) in
                if let vc = self.storyboard?.instantiateViewControllerWithIdentifier("AZBrowserController") as? AZBrowserController {
                    vc.openURL = NSBundle.mainBundle().URLForResource("Privacy", withExtension: "html")
                    vc.navigationItem.title = NSLocalizedString("Privacy Policy", comment: "")
                    vc.navigationItem.leftBarButtonItem = nil
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }))
            alertController.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .Cancel, handler: nil))
            
//            if UIDevice.currentDevice().userInterfaceIdiom == .Pad {
//                let popOver = alertController.popoverPresentationController
//                popOver?.sourceView = tableView.cellForRowAtIndexPath(indexPath)
//                popOver?.sourceRect = tableView.rectForRowAtIndexPath(indexPath)
//                popOver?.permittedArrowDirections = .Any
//
//
//            }
            
            
            presentViewController(alertController, animated: true, completion: nil)
            
            
            
            
        } else {
            let cell = tableView.cellForRowAtIndexPath(indexPath)!
            for view in cell.contentView.subviews {
                if let textField = view as? UITextField {
                    if textField.isFirstResponder() == false {
                        textField.becomeFirstResponder()
                    }
                }
                else if let textView = view as? UITextView {
                    if textView.isFirstResponder() == false {
                        textView.becomeFirstResponder()
                    }
                }
            }
        }
        
        
        
    }
}

//   MARK: -
extension AZBookingController: UITextFieldDelegate {
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        switch textField {
        case firstNameTestField:
            lastNameTestField.becomeFirstResponder()
        case lastNameTestField:
            emailTextField.becomeFirstResponder()
        case emailTextField:
            phoneTestField.becomeFirstResponder()
        case phoneTestField:
            landTestField.becomeFirstResponder()
        case landTestField:
            cityTestField.becomeFirstResponder()
        case cityTestField:
            zipTestField.becomeFirstResponder()
        default: break
        }
        
        return true
    }
}

extension String {
    var isValidEmail: Bool {
        do {
            let regex = try NSRegularExpression(pattern: "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$", options: .CaseInsensitive)
            return regex.firstMatchInString(self, options: NSMatchingOptions(rawValue: 0), range: NSMakeRange(0, self.characters.count)) != nil
        } catch {
            return false
        }
    }
}


