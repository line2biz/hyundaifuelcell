//
//  AZMenuController.swift
//  FuelCell
//
//  Created by Шурик on 06.05.16.
//  Copyright © 2016 Alex. All rights reserved.
//

import UIKit

struct MenuItem {
    
    
    typealias CompletionHandler = (() -> ())
    
    var title: String?
    private var imageName: String?
    private var completion: CompletionHandler?
    
    var image: UIImage? {
        get {
            if let name = imageName {
                return UIImage(named: name)
            } else {
                return nil
            }
        }
    }
    
    init(title: String, imageName: String, completion: CompletionHandler?) {
        self.title = title
        self.imageName = imageName
        self.completion = completion
    }
    
    
}

class AZMenuController: UIViewController {
    
    
//    MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!
    
    
//    MARK: - Properties
    var repository: Array<MenuItem> = []
    
//    MARK: - Initialization
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.commonInit()
    }
    
    override init(nibName nibNameOrNil: String!, bundle nibBundleOrNil: NSBundle!)  {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        self.commonInit()
    }
    
    func commonInit() {
        self.modalPresentationStyle = .Custom
        self.transitioningDelegate = self
    }

//    MARK: - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        // Cofigure data source
        
        repository.append(MenuItem(title: NSLocalizedString("Home", comment: ""), imageName: "Icon-Home-White", completion: {
            if let navController = self.presentingViewController as? UINavigationController {
                if let vc = self.storyboard?.instantiateViewControllerWithIdentifier("AZDashboardController") as? AZDashboardController {
                    vc.navigationItem.title = NSLocalizedString("Home", comment: "")
                    navController.viewControllers = [vc]
                    let value = UIInterfaceOrientation.Portrait.rawValue
                    UIDevice.currentDevice().setValue(value, forKey: "orientation")
                    
                }
            }
        }))
        

        repository.append(MenuItem(title: NSLocalizedString("Hydrogen Refuelling Stations", comment: ""), imageName: "Icon-Filling", completion: {
            if let navController = self.presentingViewController as? UINavigationController {
                if let vc = self.storyboard?.instantiateViewControllerWithIdentifier("AZMapController") as? AZMapController {
                    vc.navigationItem.title = NSLocalizedString("Hydrogen Refuelling Stations", comment: "")
                    vc.annotationType = .Station
                    navController.viewControllers = [vc]
                }
            }
        }))
        
        repository.append(MenuItem(title: NSLocalizedString("Fuel Cell Dealers", comment: ""), imageName: "Icon-User", completion: {
            if let navController = self.presentingViewController as? UINavigationController {
                if let vc = self.storyboard?.instantiateViewControllerWithIdentifier("AZMapController") as? AZMapController {
                    vc.navigationItem.title = NSLocalizedString("Fuel Cell Dealers", comment: "")
                    vc.annotationType = .Dealer
                    navController.viewControllers = [vc]
                }
            }
        }))
        
        repository.append(MenuItem(title: NSLocalizedString("Contact Us", comment: ""), imageName: "Icon-Drive", completion: {
            if let navController = self.presentingViewController as? UINavigationController {
                if let vc = self.storyboard?.instantiateViewControllerWithIdentifier("AZBookingController") as? AZBookingController {
                    vc.navigationItem.title = NSLocalizedString("Contact Us", comment: "")
                    navController.viewControllers = [vc]
                }
            }
        }))
        
        
        repository.append(MenuItem(title: NSLocalizedString("News", comment: ""), imageName: "Icon-News", completion: {
            if let navController = self.presentingViewController as? UINavigationController {
                if let vc = self.storyboard?.instantiateViewControllerWithIdentifier("AZBrowserController") as? AZBrowserController {
                    vc.openURL = NSURL(string: "https://www.hyundai.news/eu/technology/0/Fuel%20Cell/")
                    vc.navigationItem.title = NSLocalizedString("News", comment: "")
                    navController.viewControllers = [vc]
                }
            }
            
            
        
        }))
        repository.append(MenuItem(title: NSLocalizedString("Fuel Cell Information", comment: ""), imageName: "Icon-Info", completion: {
            if let navController = self.presentingViewController as? UINavigationController {
                
                if let vc = self.storyboard?.instantiateViewControllerWithIdentifier("AZBrowserController") as? AZBrowserController {
                    vc.openURL = NSURL(string: "http://www.fuelcell.eu")
                    vc.navigationItem.title = NSLocalizedString("Fuel Cell Information", comment: "")
                    navController.viewControllers = [vc]
                }
                
            }
        
        }))
        
        
        
        repository.append(MenuItem(title: NSLocalizedString("Impressum", comment: ""), imageName: "Icon-Impressum", completion: {
            
            if let navController = self.presentingViewController as? UINavigationController {
                if let vc = self.storyboard?.instantiateViewControllerWithIdentifier("AZBrowserController") as? AZBrowserController {
                    vc.openURL = NSBundle.mainBundle().URLForResource("Impressum", withExtension: "html")
                    vc.navigationItem.title = NSLocalizedString("Impressum", comment: "")
                    navController.viewControllers = [vc]
                }
                
            }
        }))
        
        repository.append(MenuItem(title: NSLocalizedString("Terms and Conditions", comment: ""), imageName: "Icon-Terms-White", completion: {
            
            if let navController = self.presentingViewController as? UINavigationController {
                if let vc = self.storyboard?.instantiateViewControllerWithIdentifier("AZBrowserController") as? AZBrowserController {
                    vc.openURL = NSBundle.mainBundle().URLForResource("Terms", withExtension: "html")
                    vc.navigationItem.title = NSLocalizedString("Terms and Conditions", comment: "")
                    navController.viewControllers = [vc]
                }
                
            }
        }))
        
        repository.append(MenuItem(title: NSLocalizedString("Privacy Policy", comment: ""), imageName: "Icon-Privacy-White", completion: {
            
            if let navController = self.presentingViewController as? UINavigationController {
                if let vc = self.storyboard?.instantiateViewControllerWithIdentifier("AZBrowserController") as? AZBrowserController {
                    vc.openURL = NSBundle.mainBundle().URLForResource("Privacy", withExtension: "html")
                    vc.navigationItem.title = NSLocalizedString("Privacy Policy", comment: "")
                    navController.viewControllers = [vc]
                }
                
            }
        }))
        
        
        
    }
    
    
//    MARK: - Rotation
    override func shouldAutorotate() -> Bool {
//        if let navController = self.presentingViewController as? UINavigationController {
//            return navController.visibleViewController!.shouldAutorotate()
//        }
        return true
    }
    
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
//        if let navController = self.presentingViewController as? UINavigationController {
//            return (navController.visibleViewController?.supportedInterfaceOrientations())!
//        }
        return UIInterfaceOrientationMask.Portrait
    }

    
//    MARK: - Outlet methods
    @IBAction func handleTapRecognizer(sender: UITapGestureRecognizer) {
        
        if sender.state == .Ended {
            let location = sender.locationInView(tableView)
            if tableView.indexPathForRowAtPoint(location) == nil {
                dismissViewControllerAnimated(true, completion: nil)
            }
        }
        
    }


}



//    MARK: - Table View datasource
extension AZMenuController: UITableViewDataSource {
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return repository.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! AZMenuTableCell
        let item = repository[indexPath.row]
        cell.titleMenu.text = item.title
        cell.iconImageView.image = item.image
        
        return cell
    }
}

//    MARK: - Table View delegate
extension AZMenuController: UITableViewDelegate {
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
       
        
        defer {
            dismissViewControllerAnimated(true) {
                // Do something
            }
        }
        
        if let completion = repository[indexPath.row].completion {
            completion()
        }
        
//        let identifier = repository[indexPath.row].identifier
//        guard identifier.characters.count > 0 else { return }
//        
//        
//        
//        if let navController = presentingViewController as? UINavigationController {
//            if let vc = storyboard?.instantiateViewControllerWithIdentifier(identifier) {
//                navController.viewControllers = [vc]
//            }
//        }
    }
}


//    MARK: - Transitioning Delegate
extension AZMenuController: UIViewControllerTransitioningDelegate {
    func presentationControllerForPresentedViewController(presented: UIViewController, presentingViewController presenting: UIViewController, sourceViewController source: UIViewController) -> UIPresentationController? {
        
        let presentation = AZMenuPresentationController(presentedViewController: presented, presentingViewController: presenting)
        return presentation
    }
    
    func animationControllerForPresentedController(presented: UIViewController, presentingController presenting: UIViewController, sourceController source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        let animationController = AZMenuAnimatedTransitioning()
        animationController.isPresentation = true
        return animationController
    }
    
    func animationControllerForDismissedController(dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        let animationController = AZMenuAnimatedTransitioning()
        animationController.isPresentation = false
        return animationController
    }
}